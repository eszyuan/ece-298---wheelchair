#ifndef MAIN_H_
#define MAIN_H_

#include "driverlib/driverlib.h"
#include <stdio.h>
#include <string.h>
#include <math.h>

#define TIMER_A_PERIOD  1000 //T = 1/f = (TIMER_A_PERIOD * 1 us)
#define HIGH_COUNT      500  //Number of cycles signal is high (Duty Cycle = HIGH_COUNT / TIMER_A_PERIOD)

//LaunchPad LED1 - note unavailable if UART is used
#define LED1_PORT       GPIO_PORT_P1
#define LED1_PIN        GPIO_PIN0
//LaunchPad LED2
#define LED2_PORT       GPIO_PORT_P4
#define LED2_PIN        GPIO_PIN0
//LaunchPad Pushbutton Switch 1
#define SW1_PORT        GPIO_PORT_P1
#define SW1_PIN         GPIO_PIN2
//LaunchPad Pushbutton Switch 2
#define SW2_PORT        GPIO_PORT_P2
#define SW2_PIN         GPIO_PIN6

// J1 PINS //
#define MS0_PORT           GPIO_PORT_P8
#define MS0_PIN            GPIO_PIN1

#define MS1_PORT           GPIO_PORT_P1
#define MS1_PIN            GPIO_PIN1

#define HE1_PORT           GPIO_PORT_P1
#define HE1_PIN            GPIO_PIN0

#define HE2_PORT           GPIO_PORT_P2
#define HE2_PIN            GPIO_PIN7

#define ROW2_PORT          GPIO_PORT_P8
#define ROW2_PIN           GPIO_PIN0

#define ROW3_PORT          GPIO_PORT_P5
#define ROW3_PIN           GPIO_PIN1

#define COL3_PORT          GPIO_PORT_P2
#define COL3_PIN           GPIO_PIN5

#define ROW4_PORT          GPIO_PORT_P8
#define ROW4_PIN           GPIO_PIN2

#define COL1_PORT          GPIO_PORT_P8
#define COL1_PIN           GPIO_PIN3

// J2 PINS //
#define PWMB_PORT          GPIO_PORT_P1
#define PWMB_PIN           GPIO_PIN7

#define BIN2_PORT          GPIO_PORT_P1
#define BIN2_PIN           GPIO_PIN6

#define BIN1_PORT          GPIO_PORT_P5
#define BIN1_PIN           GPIO_PIN0

#define AIN1_PORT          GPIO_PORT_P5
#define AIN1_PIN           GPIO_PIN2

#define AIN2_PORT          GPIO_PORT_P5
#define AIN2_PIN           GPIO_PIN3

#define PWMA_PORT          GPIO_PORT_P1
#define PWMA_PIN           GPIO_PIN3

#define COL2_PORT          GPIO_PORT_P1
#define COL2_PIN           GPIO_PIN4

#define ROW1_PORT          GPIO_PORT_P1
#define ROW1_PIN           GPIO_PIN5

volatile float distance = 0;
enum direction {STOPPED, FORWARD, REVERSE, LEFT, RIGHT};
volatile enum direction curState = STOPPED;
volatile enum direction tempState = STOPPED;
volatile float totalDist = 0;
volatile float speed = 0;

volatile uint16_t rev_value;

Timer_A_initUpModeParam initUpParam0 = { 0 };

volatile uint16_t listening_for_falling_edge = 1;

char* getStateStr(enum direction curState);

void Init_GPIO(void);
void Init_Clock(void);
void Init_UART(void);
void Init_PWM(void);
void Init_ADC(void);
void showSix(char *msg);
void Timer_Init(void);
void run(void);

void showStrDistCM(char s[3], int dist_cm);

void redLED();
void orangeLED();
void yellowLED();
void greenLED();

Timer_A_outputPWMParam param; //Timer configuration data structure for PWM

#endif /* MAIN_H_ */