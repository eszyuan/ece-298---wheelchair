#include "main.h"
#include "hal_LCD.h"

char ADCState = 0; //Busy state of the ADC
int16_t ADCResult = 0; //Storage for the ADC conversion result

void main(void)
{
    //Turn off interrupts during initialization
    __disable_interrupt();

    //Stop watchdog timer unless you plan on using it
    WDT_A_hold(WDT_A_BASE);

    // Initializations - see functions for more detail
    Init_GPIO();    //Sets all pins to output low as a default
//    Init_PWM();     //Sets up a PWM output
//    Init_ADC();     //Sets up the ADC to sample
    Init_Clock();   //Sets up the necessary system clocks

    Timer_Init();
//    Init_UART();    //Sets up an echo over a COM port
    Init_LCD();     //Sets up the LaunchPad LCD display

    PMM_unlockLPM5(); //Disable the GPIO power-on default high-impedance mode to activate previously configured port settings

    GPIO_clearInterrupt(HE2_PORT, HE2_PIN);
    GPIO_setAsPeripheralModuleFunctionInputPin(HE1_PORT, HE1_PIN, GPIO_SECONDARY_MODULE_FUNCTION);
//    GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P1, GPIO_PIN0);

//        GPIO_setAsPeripheralModuleFunctionInputPin(HE2_PORT, HE2_PIN, GPIO_SECONDARY_MODULE_FUNCTION);
    GPIO_setAsInputPinWithPullUpResistor(HE2_PORT, HE2_PIN);
//    GPIO_setAsInputPinWithPullUpResistor(HE1_PORT, HE1_PIN);

    GPIO_setAsPeripheralModuleFunctionOutputPin(PWMA_PORT, PWMA_PIN, GPIO_SECONDARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(PWMB_PORT, PWMB_PIN, GPIO_SECONDARY_MODULE_FUNCTION);
    //All done initializations - turn interrupts back on.
    __enable_interrupt();

    displayScrollText("ECE 298");

    GPIO_enableInterrupt(HE2_PORT, HE2_PIN);
    GPIO_selectInterruptEdge(HE2_PORT, HE2_PIN, GPIO_HIGH_TO_LOW_TRANSITION);

    redLED();

    curState = STOPPED;
    run();

    showStr6("SETDST", SET);

    while(1) //Do this when you want an infinite loop of code
    {
        GPIO_setAsInputPinWithPullDownResistor(COL1_PORT, COL1_PIN); //  COL1 - pin 4
        GPIO_setAsInputPinWithPullDownResistor(COL2_PORT, COL2_PIN); //  COL2 - pin 2
        GPIO_setAsInputPinWithPullDownResistor(COL3_PORT, COL3_PIN); //  COL3 - pin 6

// ROW 1
        GPIO_setOutputHighOnPin(ROW1_PORT, ROW1_PIN); // set ROW1 low

        if(GPIO_getInputPinValue(COL1_PORT, COL1_PIN)){ // check COL1
            GPIO_setOutputLowOnPin(ROW1_PORT, ROW1_PIN); // set ROW1 high
        }

        if(GPIO_getInputPinValue(COL2_PORT, COL2_PIN)){ // check COL2
            if(curState == STOPPED){
                totalDist++;
                if(totalDist > 999){
                    totalDist = 999;
                }
                showInt(totalDist, SET);
                showChar('D', pos1, SET);
            } else if (curState != STOPPED){
                curState = FORWARD;
                run();
            }
            GPIO_setOutputLowOnPin(ROW1_PORT, ROW1_PIN); // set ROW1 high
        }

        if(GPIO_getInputPinValue(COL3_PORT, COL3_PIN)){ // check COL3
            GPIO_setOutputLowOnPin(ROW1_PORT, ROW1_PIN); // set ROW1 high
        }

        GPIO_setOutputLowOnPin(ROW1_PORT, ROW1_PIN); // set ROW1 high
// END ROW 1

// ROW 2
        GPIO_setOutputHighOnPin(ROW2_PORT, ROW2_PIN); // set ROW2 low

        if(GPIO_getInputPinValue(COL1_PORT, COL1_PIN)){ // check COL1
            if(curState != STOPPED){
                curState = LEFT;
                run();
            }
            GPIO_setOutputLowOnPin(ROW2_PORT, ROW2_PIN); // set ROW2 low
        }

        if(GPIO_getInputPinValue(COL2_PORT, COL2_PIN)){ // check COL2
            if(curState == STOPPED){
                curState = FORWARD;
            } else if (curState != STOPPED){
                curState = STOPPED;
                showInt(totalDist, SET);
                showChar('D', pos1, SET);
            }
            run();
            GPIO_setOutputLowOnPin(ROW2_PORT, ROW2_PIN); // set ROW2 low
        }

        if(GPIO_getInputPinValue(COL3_PORT, COL3_PIN)){ // check COL3
            if(curState != STOPPED){
                curState = RIGHT;
                run();
            }
            GPIO_setOutputLowOnPin(ROW2_PORT, ROW2_PIN); // set ROW2 low
        }

        GPIO_setOutputLowOnPin(ROW2_PORT, ROW2_PIN); // set ROW2 low
// END ROW 2

// ROW 3
        GPIO_setOutputHighOnPin(ROW3_PORT, ROW3_PIN); // set ROW3 low

        if(GPIO_getInputPinValue(COL1_PORT, COL1_PIN)){ // check COL1
            GPIO_setOutputLowOnPin(ROW3_PORT, ROW3_PIN); // set ROW3 high
        }

        if(GPIO_getInputPinValue(COL2_PORT, COL2_PIN)){ // check COL2
            if(curState == STOPPED){
                totalDist--;
                if(totalDist < 0){
                    totalDist = 0;
                }
                showInt(totalDist, SET);
                showChar('D', pos1, SET);
            } else if (curState != STOPPED){
                curState = REVERSE;
                run();
            }
            GPIO_setOutputLowOnPin(ROW3_PORT, ROW3_PIN); // set ROW3 high
        }

        if(GPIO_getInputPinValue(COL3_PORT, COL3_PIN)){ // check COL3
            GPIO_setOutputLowOnPin(ROW3_PORT, ROW3_PIN); // set ROW3 high
        }

        GPIO_setOutputLowOnPin(ROW3_PORT, ROW3_PIN); // set ROW3 high
// END ROW 3

// ROW 4
        GPIO_setOutputHighOnPin(ROW4_PORT, ROW4_PIN); // set ROW4 low
        if(GPIO_getInputPinValue(COL1_PORT, COL1_PIN)){ // check COL1
            GPIO_setOutputLowOnPin(ROW4_PORT, ROW4_PIN); // set ROW4 high
        }

        if(GPIO_getInputPinValue(COL2_PORT, COL2_PIN)){ // check COL2
            if(curState == STOPPED){
                totalDist = 0;
                showInt(totalDist, SET);
                showChar('D', pos1, SET);
            }
            GPIO_setOutputLowOnPin(ROW4_PORT, ROW4_PIN); // set ROW4 high
        }

        if(GPIO_getInputPinValue(COL3_PORT, COL3_PIN)){ // check COL3
            GPIO_setOutputLowOnPin(ROW4_PORT, ROW4_PIN); // set ROW4 high
        }

        GPIO_setOutputLowOnPin(ROW4_PORT, ROW4_PIN); // set ROW4 high
//    // END ROW 4
    }
}

//PORT1 interrupt vector service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT2_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(PORT2_VECTOR)))
#endif

void P2_ISR(void)
{
    //Start timer on rising edge, stop on falling edge, print counter value

    __disable_interrupt();

    if (listening_for_falling_edge == 1)
    {
        Timer_A_initUpMode(TIMER_A0_BASE, &initUpParam0);
        Timer_A_startCounter(TIMER_A0_BASE, TIMER_A_UP_MODE);
        listening_for_falling_edge = 0;
        GPIO_selectInterruptEdge(HE2_PORT, HE2_PIN,
                                 GPIO_HIGH_TO_LOW_TRANSITION);
    }
    else
    {
        Timer_A_stop(TIMER_A0_BASE);


        listening_for_falling_edge = 1;
        GPIO_selectInterruptEdge(HE2_PORT, HE2_PIN,
                                 GPIO_LOW_TO_HIGH_TRANSITION);

        GPIO_disableInterrupt(HE2_PORT, HE2_PIN);

        if(curState == FORWARD){
            rev_value = Timer_A_getCounterValue(TIMER_A0_BASE);
            speed = 6.5 * 15625 * 3.14159/ rev_value;

            distance += 6.5 * 3.14159;
            showInt((int)speed, SET);
            showChar('S', pos1, SET);
        } else {
            showInt(0, SET);
            showChar('S', pos1, SET);
        }


       if((totalDist - distance) > ((totalDist * 2)/3)){
           greenLED();
       }
       if((totalDist - distance) > (totalDist/3) && (totalDist - distance) < ((totalDist * 2)/3)){
           yellowLED();
       }
       if((totalDist - distance) < (totalDist/3) && (totalDist - distance) > 0){
           orangeLED();
       }
       if(totalDist - distance <= 0){
           redLED();
           curState = STOPPED;
           distance = 0;
           run();
       }

        GPIO_enableInterrupt(HE2_PORT, HE2_PIN);
    }

    GPIO_clearInterrupt(HE2_PORT, HE2_PIN);
    __enable_interrupt();
}

void Init_GPIO(void)
{
    // Set all GPIO pins to output low to prevent floating input and reduce power consumption
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P6, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P6, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

    //Set LaunchPad switches as inputs - they are active low, meaning '1' until pressed
    GPIO_setAsInputPinWithPullUpResistor(SW1_PORT, SW1_PIN);
    GPIO_setAsInputPinWithPullUpResistor(SW2_PORT, SW2_PIN);

    //Set LED1 and LED2 as outputs
    //GPIO_setAsOutputPin(LED1_PORT, LED1_PIN); //Comment if using UART
    GPIO_setAsOutputPin(LED2_PORT, LED2_PIN);
}

/* Clock System Initialization */
void Init_Clock(void)
{
    //Set P4.1 and P4.2 as Primary Module Function Input, XT_LF
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P4, GPIO_PIN1 + GPIO_PIN2, GPIO_PRIMARY_MODULE_FUNCTION);

    // Set external clock frequency to 32.768 KHz
    CS_setExternalClockSource(32768);
    // Set ACLK = XT1
    CS_initClockSignal(CS_ACLK, CS_XT1CLK_SELECT, CS_CLOCK_DIVIDER_1);
    // Initializes the XT1 crystal oscillator
    CS_turnOnXT1LF(CS_XT1_DRIVE_1);
    // Set SMCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_SMCLK, CS_DCOCLKDIV_SELECT, CS_CLOCK_DIVIDER_1);
    // Set MCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_MCLK, CS_DCOCLKDIV_SELECT, CS_CLOCK_DIVIDER_1);
}

/* UART Initialization */
void Init_UART(void)
{
    //Configure UART pins, which maps them to a COM port over the USB cable
    //Set P1.0 and P1.1 as Secondary Module Function Input.
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_PRIMARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN0, GPIO_PRIMARY_MODULE_FUNCTION);

    //SMCLK = 1MHz, Baudrate = 9600
    //UCBRx = 6, UCBRFx = 8, UCBRSx = 17, UCOS16 = 1
    EUSCI_A_UART_initParam param = {0};
        param.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
        param.clockPrescalar    = 6;
        param.firstModReg       = 8;
        param.secondModReg      = 17;
        param.parity            = EUSCI_A_UART_NO_PARITY;
        param.msborLsbFirst     = EUSCI_A_UART_LSB_FIRST;
        param.numberofStopBits  = EUSCI_A_UART_ONE_STOP_BIT;
        param.uartMode          = EUSCI_A_UART_MODE;
        param.overSampling      = 1;

    if(STATUS_FAIL == EUSCI_A_UART_init(EUSCI_A0_BASE, &param))
    {
        return;
    }

    EUSCI_A_UART_enable(EUSCI_A0_BASE);

    EUSCI_A_UART_clearInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);

    // Enable EUSCI_A0 RX interrupt
    EUSCI_A_UART_enableInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);
}

/* EUSCI A0 UART ISR - Echoes data back to PC host */
#pragma vector=USCI_A0_VECTOR
__interrupt
void EUSCIA0_ISR(void)
{
    uint8_t RxStatus = EUSCI_A_UART_getInterruptStatus(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG);

    EUSCI_A_UART_clearInterrupt(EUSCI_A0_BASE, RxStatus);

    if (RxStatus)
    {
        EUSCI_A_UART_transmitData(EUSCI_A0_BASE, EUSCI_A_UART_receiveData(EUSCI_A0_BASE));
    }
}

void redLED(){
    // A3, S: 11
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P8, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);

    GPIO_setOutputHighOnPin(MS1_PORT, MS0_PIN);
    GPIO_setOutputHighOnPin(MS0_PORT, MS0_PIN);

    GPIO_setAsOutputPin(MS1_PORT, MS0_PIN);
    GPIO_setAsOutputPin(MS0_PORT, MS0_PIN);

    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P8, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
}

void orangeLED(){
    // A0, S: 00
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P8, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);

    GPIO_setOutputLowOnPin(MS1_PORT, MS0_PIN);
    GPIO_setOutputLowOnPin(MS0_PORT, MS0_PIN);

    GPIO_setAsOutputPin(MS1_PORT, MS0_PIN);
    GPIO_setAsOutputPin(MS0_PORT, MS0_PIN);

    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P8, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
}

void yellowLED(){
    // A1, S: 01
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P8, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);

    GPIO_setOutputLowOnPin(MS1_PORT, MS0_PIN);
    GPIO_setOutputHighOnPin(MS0_PORT, MS0_PIN);

    GPIO_setAsOutputPin(MS1_PORT, MS0_PIN);
    GPIO_setAsOutputPin(MS0_PORT, MS0_PIN);

    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P8, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
}

void greenLED(){
    // A2, S: 10
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P8, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);

    GPIO_setOutputHighOnPin(MS1_PORT, MS0_PIN);
    GPIO_setOutputLowOnPin(MS0_PORT, MS0_PIN);

    GPIO_setAsOutputPin(MS1_PORT, MS0_PIN);
    GPIO_setAsOutputPin(MS0_PORT, MS0_PIN);

    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P8, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_SECONDARY_MODULE_FUNCTION);
}


char* getStateStr(enum direction state){
    switch (state){
    case STOPPED:
        return "STP";
    case FORWARD:
        return "FWD";
    case REVERSE:
        return "REV";
    case LEFT:
        return "L  ";
    case RIGHT:
        return "R  ";
    }
    return "";
}

void run(void){
    switch(curState){
    case STOPPED:
        GPIO_setOutputHighOnPin(AIN1_PORT, AIN1_PIN);
        GPIO_setOutputLowOnPin(AIN2_PORT, AIN2_PIN);
        GPIO_setOutputLowOnPin(PWMA_PORT, PWMA_PIN);

        GPIO_setOutputLowOnPin(BIN1_PORT, BIN1_PIN);
        GPIO_setOutputHighOnPin(BIN2_PORT, BIN2_PIN);
        GPIO_setOutputLowOnPin(PWMB_PORT, PWMB_PIN);
        break;
    case FORWARD:
        GPIO_setOutputHighOnPin(AIN1_PORT, AIN1_PIN);
        GPIO_setOutputLowOnPin(AIN2_PORT, AIN2_PIN);
        GPIO_setOutputHighOnPin(PWMA_PORT, PWMA_PIN);

        GPIO_setOutputLowOnPin(BIN1_PORT, BIN1_PIN);
        GPIO_setOutputHighOnPin(BIN2_PORT, BIN2_PIN);
        GPIO_setOutputHighOnPin(PWMB_PORT, PWMB_PIN);
        break;
    case REVERSE:
        GPIO_setOutputLowOnPin(AIN1_PORT, AIN1_PIN);
        GPIO_setOutputHighOnPin(AIN2_PORT, AIN2_PIN);
        GPIO_setOutputHighOnPin(PWMA_PORT, PWMA_PIN);

        GPIO_setOutputHighOnPin(BIN1_PORT, BIN1_PIN);
        GPIO_setOutputLowOnPin(BIN2_PORT, BIN2_PIN);
        GPIO_setOutputHighOnPin(PWMB_PORT, PWMB_PIN);
        break;
    case LEFT:
        GPIO_setOutputHighOnPin(AIN1_PORT, AIN1_PIN);
        GPIO_setOutputLowOnPin(AIN2_PORT, AIN2_PIN);
        GPIO_setOutputLowOnPin(PWMA_PORT, PWMA_PIN);

        GPIO_setOutputLowOnPin(BIN1_PORT, BIN1_PIN);
        GPIO_setOutputHighOnPin(BIN2_PORT, BIN2_PIN);
        GPIO_setOutputHighOnPin(PWMB_PORT, PWMB_PIN);
        break;
    case RIGHT:
        GPIO_setOutputHighOnPin(AIN1_PORT, AIN1_PIN);
        GPIO_setOutputLowOnPin(AIN2_PORT, AIN2_PIN);
        GPIO_setOutputHighOnPin(PWMA_PORT, PWMA_PIN);

        GPIO_setOutputLowOnPin(BIN1_PORT, BIN1_PIN);
        GPIO_setOutputHighOnPin(BIN2_PORT, BIN2_PIN);
        GPIO_setOutputLowOnPin(PWMB_PORT, PWMB_PIN);
        break;
    default:
        break;
    }
}

//ADC interrupt service routine
#pragma vector=ADC_VECTOR
__interrupt
void ADC_ISR(void)
{
    uint8_t ADCStatus = ADC_getInterruptStatus(ADC_BASE, ADC_COMPLETED_INTERRUPT_FLAG);

    ADC_clearInterrupt(ADC_BASE, ADCStatus);

    if (ADCStatus)
    {
        ADCState = 0; //Not busy anymore
        ADCResult = ADC_getResults(ADC_BASE);
    }
}

void Timer_Init(void){
    //Initialize Timer 0 - For timing the length of echo pulse
    initUpParam0.clockSource = TIMER_A_CLOCKSOURCE_SMCLK;
    initUpParam0.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_64;
    initUpParam0.timerPeriod = 0xFFFF;
    initUpParam0.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_DISABLE;
    initUpParam0.timerClear = TIMER_A_DO_CLEAR;
    initUpParam0.startTimer = false;
    Timer_A_initUpMode(TIMER_A0_BASE, &initUpParam0);
    Timer_A_initUpMode(TIMER_A1_BASE, &initUpParam0);
}
